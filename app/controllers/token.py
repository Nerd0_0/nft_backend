from random import choice
from string import ascii_lowercase, digits

from aiohttp import ClientResponseError
from fastapi import APIRouter, HTTPException
from sqlalchemy import select

from config import PRIVATE_KEY
from db import get_db_session
from db.models.token import Token
from app import schemas
from ethereum.contract import w3, contact_abi

router = APIRouter(prefix="/tokens", tags=["token"])

# TODO: use singleton for sure contract alone in project
w3_contract = w3.eth.contract(
    address="0xcFcB050e965115A8F7922D617A39Ea577bcaC2C1", abi=contact_abi
)

# TODO: decorator for catching db or aiohttp errors


@router.post("/create")
async def create_token(media_url: str, owner: str) -> schemas.Token:
    """
    Create nft token, send to someone and save in db
    """
    unique_hash = "".join(choice(ascii_lowercase + digits) for _ in range(20))

    # TODO: need login
    await w3_contract.functions.mint(
        owner=owner, uniqueHash=unique_hash, mediaURL=media_url
    ).call()

    tx_hash = w3.eth.send_transaction({
        "from": (await w3.eth.accounts)[0],
        "to": owner,
        "value": media_url
    })

    token = Token(media_url=media_url, owner=owner, unique_hash=unique_hash, tx_hash=tx_hash)
    async_session = get_db_session()
    async with async_session() as session:
        async with async_session.begin():
            session.add(token)
            await session.commit()

    return schemas.Token(
        id=token.id,
        unique_hash=token.unique_hash,
        media_url=token.media_url,
        owner=token.owner,
        tx_hash=token.tx_hash,
    )


@router.get("/list")
async def get_token_list() -> list[schemas.Token]:
    """
    Get token list
    """
    async_session = get_db_session()
    async with async_session() as session:
        tokens = (await session.execute(select(Token))).scalars()
        return [
            schemas.Token(
                id=token.id,
                unique_hash=token.unique_hash,
                media_url=token.media_url,
                owner=token.owner,
                tx_hash=token.tx_hash,
            )
            for token in tokens
        ]


@router.get("/total_supply")
async def get_token_total_supply() -> schemas.TokenSupply:
    """
    Get some data about tokens
    """
    # TODO: need login
    try:
        total_supply = await w3_contract.functions.totalSupply().call()
        return schemas.TokenSupply(result=total_supply)
    except ClientResponseError as exc:
        raise HTTPException(status_code=exc.status, detail=exc.message)
