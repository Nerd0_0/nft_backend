import uvicorn
from fastapi import FastAPI, APIRouter
from app.controllers.token import router as controller_router
from config import SERVER_HOST, SERVER_PORT
from ethereum.contract import w3

router = APIRouter()
router.include_router(controller_router)


app = FastAPI(title="ethereum-some", docs_url="/ui")
app.include_router(router)


@app.on_event("startup")
async def startup_event():
    assert await w3.is_connected()


if __name__ == "__main__":
    uvicorn.run(app, host=SERVER_HOST, port=SERVER_PORT)
