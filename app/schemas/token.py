# pylint: disable=too-few-public-methods,no-name-in-module
from pydantic import BaseModel


class Token(BaseModel):
    id: int
    unique_hash: str
    media_url: str
    owner: str
    tx_hash: str | None = None
