FROM python:latest
RUN mkdir /application
WORKDIR /application

COPY requirements.txt /application/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /application

EXPOSE 8000
