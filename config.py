import os

POSTGRES_PROTOCOL = "postgresql+asyncpg"
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "127.0.0.1")
POSTGRES_PORT = int(os.getenv("POSTGRES_PORT", "5432"))
POSTGRES_USER = os.getenv("POSTGRES_USER", "token_db")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD", "easy_pass")
POSTGRES_DB = os.getenv("POSTGRES_DB", "token_db")

SQLALCHEMY_DATABASE_URI = (
    f"{POSTGRES_PROTOCOL}://{POSTGRES_USER}:{POSTGRES_PASSWORD}@"
    f"{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}"
)

# FixMe: where infura key?
INFURA_KEY = "0x0000000000000000000000000000000000000000000000000000000000000000"
WEB3_PROVIDER = f"https://infura.io/v3/{INFURA_KEY}"
# WEB3_PROVIDER = "https://docs.moralis.io/speedy-nodes/connecting-to-rpc-nodes/connect-to-eth-node"

SERVER_HOST = os.getenv("SERVER_HOST", "127.0.0.1")
SERVER_PORT = int(os.getenv("SERVER_PORT", "8000"))
PRIVATE_KEY = "some_key"