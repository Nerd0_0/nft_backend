from sqlalchemy import Column, Integer, String

from db.session import Base


class Token(Base):
    __tablename__ = "nft_token"

    id = Column(Integer, primary_key=True)
    unique_hash = Column(String(20), unique=True)
    tx_hash = Column(String(256), nullable=True)
    media_url = Column(String(256))
    owner = Column(String(256))
