import sqlalchemy
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession, async_sessionmaker
from sqlalchemy.orm import Session, declarative_base

from config import SQLALCHEMY_DATABASE_URI

metadata = sqlalchemy.MetaData()
Base = declarative_base(metadata=metadata)


def get_db_session() -> async_sessionmaker[Session]:
    """Create async session to database from SQLALCHEMY_DATABASE_URI"""
    engine = create_async_engine(SQLALCHEMY_DATABASE_URI)
    async_session = async_sessionmaker(
        engine, expire_on_commit=False, class_=AsyncSession
    )
    return async_session
